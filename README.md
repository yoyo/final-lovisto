# Final Lo Visto

Práctica final del curso 2020/21
## Datos
* Nombre: Jose Jorge Martínez Fernández
* Correo: jj.martinezf.2017@alumnos.urjc.es
* Titulación: ING. Sistemas de Telecomunicaciones
* Despliegue (url): http://jjorgem.pythonanywhere.com/LoVisto/
* Video básico (url): https://youtu.be/KsT76Fx2-7k
* Video parte opcional (url): https://youtu.be/cDkpM-eBiDg

## Cuenta Admin Site
* admin/admin
* jorge/jorge
    
## Cuentas usuarios
* Las mismas que las del Admin Site

## Lista partes obligatoria
* La práctica realiza la parte obligatoria del proyecto,
La aplicación parsea todos los recursos conocidos exceptuando Youtube que no he
conseguido sacarlo, por ello he realizado otros dos recursos reconocidos (explicados en el video).
Por lo demás, funciona correctamente, además se puede poner modo nocturno (oscuro).
  
  
## Lista partes opcionales
* Nombre parte:  Inclusión de favicon
* Nombre parte:  Dos nuevos recursos no reconocidos
* Nombre parte:  Añadir como comentario Url de foto
* Nombre parte:  XML y JSON de la pagina principal,usuario

