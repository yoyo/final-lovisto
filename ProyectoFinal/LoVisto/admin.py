from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Recursos, Contenido, Comentario, Votar

# Register your models here.
admin.site.register(Recursos)
admin.site.register(Contenido)
admin.site.register(Comentario)
admin.site.register(Votar)