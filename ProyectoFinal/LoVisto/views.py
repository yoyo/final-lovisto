from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido, Comentario, Votar
from django.utils import timezone
from django.shortcuts import redirect, render
from django.contrib.auth import logout
from bs4 import BeautifulSoup
import requests
from django.http import JsonResponse

# Create your views here.
@csrf_exempt
def pagina_principal(request):
    modo = 0
    if request.method =="POST":
        action = request.POST['action']
        if action == "Introducir":
            try:
                c= Contenido.objects.get(clave=request.POST['clave'])
                c.delete()
            except Contenido.DoesNotExist:
                c=Contenido(clave=request.POST['clave'],
                          valor=request.POST['valor'],
                          usuario=request.user.username,
                          votos_positivos=0,
                          votos_negativos=0,
                          NComentarios=0,
                          fecha =timezone.now())
                c.save()
        elif action == "Iniciar sesion":
            return redirect('/login')

        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')

        elif action == "Pagina del Usuario":
            return redirect('user/' + request.user.username)

        elif action == "Pagina Aportaciones":
            return redirect('/LoVisto/aportaciones')

        elif action == "Informacion":
            return redirect('/LoVisto/informacion')

        elif action == "OSCURO":
            modo = 1
        elif action == "CLARO":
            modo = 0

    contenido_list = Contenido.objects.all()  #Esto es el contenido de lo nuevo que se va metiendo
    contenido_list = list(reversed(contenido_list))
    contenido_list_crop = contenido_list[0:10]
    contenido_list_piepagina = contenido_list[0:3]
    aportaciones = 10
    iterador = 0
    context={
        'contenido_list_crop':contenido_list_crop,
        'contenido_list':contenido_list,
        'numeroapor':aportaciones,
        'iterador':iterador,
        'lista_piedepagina': contenido_list_piepagina,
        'oscuro': modo
    }
    return render(request,'aplicacion/PaginaPrincipal.html',context)

@csrf_exempt
def pagina_usuario(request,llave):
    modo = 0
    if request.method =="POST":
        action=request.POST['action']
        if request.POST['action']== "Pagina Principal":
            return redirect('/LoVisto')

        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')

        elif action == "Pagina Aportaciones":
            return redirect('/LoVisto/aportaciones')

        elif action == "Informacion":
            return redirect('/LoVisto/informacion')

        elif action == "OSCURO":
            modo = 1
        elif action == "CLARO":
            modo = 0

    contenido_list = Contenido.objects.all()
    comentario_list = Comentario.objects.all()
    votos_list = Votar.objects.all()
    contenido_list_piepagina = contenido_list[0:3]
    context = {
         'contenido_list': contenido_list,
         'comentario_list': comentario_list,
         'lista_piedepagina': contenido_list_piepagina,
         'votos_list':votos_list,
         'oscuro': modo
     }
    return render(request,'aplicacion/Paginausuario.html', context)

@csrf_exempt
def pagina_aportaciones(request):
    modo = 0
    if request.method == "POST":
        action = request.POST['action']
        if request.POST['action'] == "Pagina Principal":
            return redirect('/LoVisto')

        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')

        elif action == "Iniciar sesion":
            return redirect('/login')

        elif action == "Pagina del Usuario":
            return redirect('user/' + request.user.username)

        elif action == "Informacion":
            return redirect('/LoVisto/informacion')
        elif action == "OSCURO":
            modo = 1
        elif action == "CLARO":
            modo = 0

    contenido_list = list(reversed(Contenido.objects.all()))
    comentario_list = Comentario.objects.all()
    vote_list = Votar.objects.all()
    contenido_list_piepagina = contenido_list[0:3]
    context = {
        'contenido_list': contenido_list,
        'comentario_list': comentario_list,
        'lista_piedepagina': contenido_list_piepagina,
        'votos_list': vote_list,
        'oscuro': modo
    }
    return render(request, 'aplicacion/PaginaAportaciones.html', context)

@csrf_exempt
def pagina_informacion(request):
    if request.method == "POST":
        action = request.POST['action']
        if request.POST['action'] == "Pagina Principal":
            return redirect('/LoVisto')

        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')

        elif action == "Iniciar sesion":
            return redirect('/login')

        elif action == "Pagina del Usuario":
            return redirect('user/' + request.user.username)

        elif action == "Pagina Aportaciones":
            return redirect('/LoVisto/aportaciones')
    return render(request,'aplicacion/PaginaInformacion.html',{} )



def pagina_unrecurso(request,llave):
    if request.method =="PUT":
        valor = request.body.decode('utf-8')
    if request.method =="POST":
        action = request.POST['action']
        if request.POST['action'] == "Cambiar contenido":
            try:
                c= Contenido.objects.get(clave=llave)
                c.valor=request.POST['valor']
                c.save()
            except Contenido.DoesNotExist:
                c=Contenido(clave=llave, valor=request.POST['valor'])
                c.save()
        elif request.POST['action']== "Enviar contenido":
            clave = llave
            try:
                c= Contenido.objects.get(clave=clave)
                c.delete()
            except Contenido.DoesNotExist:
                c=Contenido(clave=clave, valor=request.POST['valor'],
                          usuario=request.user.username,
                          votos_positivos=0,
                          votos_negativos=0,
                          NComentarios=0,
                          fecha=timezone.now())
                c.save()

        elif request.POST['action'] == "ME GUSTA":
            contenido = Contenido.objects.get(clave=llave)
            contenido.votos_positivos= contenido.votos_positivos+1
            contenido.save()
            votoaux=Votar(contenido=Contenido.objects.get(clave=llave),
                          usuario=request.user.username,
                          valor="megusta")
            votoaux.save()

        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')

        elif action == "Iniciar sesion":
            return redirect('/login')

        elif request.POST['action'] == "NO ME GUSTA":
            contenido= Contenido.objects.get(clave=llave)
            contenido.votos_negativos= contenido.votos_negativos+1
            contenido.save()
            votoaux = Votar(contenido=Contenido.objects.get(clave=llave),
                           usuario=request.user.username,
                           valor="nomegusta")
            votoaux.save()
        elif request.POST['action'] == "Borrar contenido":
            try:
                c= Contenido.objects.get(clave=llave)
                c.delete()
                return redirect('/LoVisto')
            except Contenido.DoesNotExist:
                c=Contenido.objects.get(clave=llave, valor = "")
                c.save()
        elif request.POST['action']== "Pagina Principal":
            return redirect('/LoVisto')

        elif action == "Pagina del Usuario":
            return redirect('user/' + request.user.username)

        elif action == "Pagina Aportaciones":
            return redirect('/LoVisto/aportaciones')

        elif action == "Informacion":
            return redirect('/LoVisto/informacion')

        elif request.POST['action']== "Borrar votacion":
            contenido= Contenido.objects.get(clave=llave)
            votoaux = Votar.objects.get(contenido=Contenido.objects.get(clave=llave),
                           usuario=request.user.username)
            if votoaux.valor=="megusta":
                contenido.votos_positivos = contenido.votos_positivos-1
            elif votoaux.valor=="nomegusta":
                contenido.votos_negativos = contenido.votos_negativos-1
            contenido.save()
            votoaux.delete()

        elif request.POST['action']== "Enviar comentario":
            contenido = Contenido.objects.get(clave=llave)
            contenido.NComentarios= contenido.NComentarios+1
            q=Comentario(contenido=contenido, titulo=request.POST['titulo'],
                         imagen=request.POST['imagen'],
                         valor=request.POST['valor'],
                         fecha=timezone.now(),
                         usuario=request.user.username)
            q.save()
            contenido.save()
    try:
        Contenido.objects.get(clave=llave)

    except Contenido.DoesNotExist:   #XML JSON
        if request.GET.get("format") == "xml/":
            contenidoxml = loader.get_template('aplicacion/XML.html')
            context = {'Contenido': Contenido.objects.all()}
            return HttpResponse(contenidoxml.render(context, request))

        if request.GET.get("format") == "json/":
            contenido = Contenido.objects.filter().values()
            return JsonResponse({"Contenido": list(contenido)})

        contenido = Contenido.objects.all()
        comentario_list = (Comentario.objects.all())
        votos_list = Votar.objects.all()
        context={
                'contenido': contenido,
                'comentario_list': comentario_list,
                'votos_list': votos_list
        }
        return render(request, 'aplicacion/PaginaRecursoError.html', context)

    contenido = Contenido.objects.get(clave=llave)
    comentario_list = (Comentario.objects.all())
    votos_list = Votar.objects.all()
    content = Contenido.objects.get(clave=llave)
    url = content.valor
    headers = {'User-Agent': 'Mozilla/5.0'}
    try:
        page = requests.get(url, headers=headers)
        soup = BeautifulSoup(page.text, 'html.parser') #Parseamos HTML

        #Para los recursos reconocidos
        if "www.reddit.com" in url:
            try:
                text = soup.find("p", class_="_1qeIAgB0cPwnLhDF9XSiJM").text
                aprobacion = soup.find("div", class_="_1rZYMD_4xY3gRcSS3p8ODO _25IkBM0rRUqWX5ZojEMAFQ").text
                title = soup.find("title").text
                context = {
                    'contenido': content,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list,
                    'texto': text,
                    'titulo': title,
                    'aprobacion': aprobacion
                }
                return render(request, 'aplicacion/Reddit.html', context)
            except AttributeError:
                context = {
                    'contenido': content,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list
                }
                return render(request, 'aplicacion/ERROR.html', context)

        elif "es.wikipedia.org" in url:
            try:
                text = soup.find(id="bodyContent").text
                images = soup.findAll('img')
                title = soup.find(id="firstHeading").text
                texto = text[0:400] #Primeros 400 caracteres
                imagen = ((images[0])['src'])
                context = {
                    'contenido': content,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list,
                    'titulo': title,
                    'texto':texto,
                    'imagen': imagen,
                }
                return render(request, 'aplicacion/Wiki.html', context)
            except AttributeError:
                context = {
                    'contenido': content,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list}
                return render(request, 'aplicacion/ERROR.html', context)

        elif "okdiario.com" in url: #Extra
            try:
                title = soup.find("title").text
                text = soup.find("p").text
                images = soup.findAll('img')
                imagen = ((images[2])['src'])
                context = {
                    'contenido': contenido,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list,
                    'titulo': title,
                    'imagen': imagen,
                    'texto': text,
                }
                return render(request, 'aplicacion/okdiario.html', context)
            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list
                }
                print("ERROR OKDIARIO")
                return render(request, 'aplicacion/ERROR.html', context)

        elif "www.kayak.es" in url: #Extra
            try:
                title = soup.find("title").text
                textida = soup.find("div", class_="keel-container s-t-bp").text
                textvuelta = soup.find("div", role_="list")
                context = {
                    'contenido': contenido,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list,
                    'titulo': title,
                    'texto': textida,
                    'texto1': textvuelta
                }
                return render(request, 'aplicacion/vuelos.html', context)
            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list
                }
                return render(request, 'aplicacion/ERROR.html', context)

        elif "www.aemet.es" in url:
            try:
                title = soup.find("title").text
                temperatura = soup.find("div", class_="no_wrap").text
                fecha = soup.find("th", class_="borde_izq_dcha_fecha").text
                maxymin = soup.find("td", class_="alinear_texto_centro no_wrap comunes").text
                precipitacion = soup.find("td", class_="nocomunes").text
                copy = soup.find("div", class_="texto_copy").text
                viento = soup.find("div", class_="imagen_viento").text
                ultravioleta = soup.find("span", class_="raduv_pred_nivel4").text
                context = {
                     'contenido': contenido,
                     'comentario_list': comentario_list,
                     'votos_list': votos_list,
                     'titulo': title,
                     'temperatura': temperatura,
                     'fecha': fecha,
                     'maxymin': maxymin,
                     'precipitacion': precipitacion,
                     'copy': copy,
                     'viento': viento,
                     'ultravioleta': ultravioleta
                }
                return render(request,'aplicacion/amet.html', context)
            except AttributeError:
                context = {
                     'contenido': contenido,
                     'comentario_list': comentario_list,
                     'votos_list': votos_list
                }
            return render(request,'aplicacion/ERROR.html', context)
        else: #Otros rescursos no reconocidos
            try:
                images = soup.findAll('img')
                title = soup.find("title").text
                imagen = ((images[1])['src'])
                context = {
                    'contenido': contenido,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list,
                    'titulo': title,
                    'imagen': imagen
                }
                return render(request, 'aplicacion/NoReconocido.html', context)

            except AttributeError:
                context = {
                    'contenido': contenido,
                    'comentario_list': comentario_list,
                    'votos_list': votos_list}
                return render(request, 'aplicacion/ERROR.html', context)
    except:
        context = {
            'contenido': contenido,
            'comentario_list': comentario_list,
            'votos_list': votos_list
        }
    return render(request, 'aplicacion/ERROR.html', context)

