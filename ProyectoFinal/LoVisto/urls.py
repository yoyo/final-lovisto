from django.urls import path
from . import views

urlpatterns = [
    path('', views.pagina_principal),
    path('user/<str:llave>', views.pagina_usuario),
    path('aportaciones', views.pagina_aportaciones),
    path('informacion', views.pagina_informacion),
    path('<str:llave>',views.pagina_unrecurso),
]