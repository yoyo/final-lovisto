from django.db import models

# Create your models here.
class Recursos(models.Model): #TOPIC el tipo de recurso que puede meter el ususario
    RECURSO = [
        ('AEMET', 'Tiempo'),
        ('WIKI', 'Wikipedia'),
        ('YT', 'Youtube'),
        ('RE', 'Reddit'),
        ('OTRO', 'Otros'),
    ]
    recursos = models.CharField(max_length=5, choices=RECURSO)
    def __str__(self):
        return str(self.id) + ": " + self.recursos

#Relacion N:N
class Contenido(models.Model):
    clave = models.CharField(max_length=128) #TITULO
    valor = models.TextField() #url
    usuario = models.TextField()
    votos_positivos = models.IntegerField()
    votos_negativos = models.IntegerField()
    fecha = models.DateTimeField('publicado') #Fecha de la aportacion
    NComentarios = models.IntegerField()  #Numero de comentarios
    info = models.TextField()
    recurso = models.ManyToManyField(Recursos)  #Lo relacionamos con la clase Recursos para que el tipo

    def __str__(self):
        return str(self.id) + ": " + self.clave


class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    usuario = models.TextField()
    valor = models.TextField()  #Comentario
    titulo = models.CharField(max_length=200)
    imagen = models.TextField(default="No url") #Optativo(Extra)
    fecha = models.DateTimeField('publicado')

    def __str__(self):
        return str(self.id) + ": " + self.valor

class Votar(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    usuario = models.TextField()
    valor = models.TextField() #Ponemos voto positivo o negativo

    def __str__(self):
        return str(self.contenido) + ": " + self.usuario